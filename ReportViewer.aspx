<%@ Register TagPrefix="RS" Namespace="Microsoft.ReportingServices.WebServer" Assembly="ReportingServicesWebServer" %>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="Microsoft.ReportingServices.WebServer.ReportViewerPage" EnableEventValidation="false" %>
<asp:literal runat="server" id="docType"></asp:literal>
<html>
 <head id="headID" runat="server">
  <asp:literal runat="server" id="httpEquiv"></asp:literal>
  <title><%= GetPageTitle() %></title>
 </head>
 <body style="margin: 0px; overflow: hidden">
  <form style="width:100%;height:100%" runat="server" ID="ReportViewerForm">
   <asp:ScriptManager ID="AjaxScriptManager" AsyncPostBackTimeout="0" runat="server" />
    <table cellspacing="0" cellpadding="0" width="100%" height="100%"><tr height="100%"><td width="100%">
   <RS:ReportViewerHost ID="ReportViewerControl" runat="server" />
    </td></tr></table>
  </form>
  <script language="javascript" type="text/javascript">
Sys.WebForms.PageRequestManager.prototype._destroyTree = function(element) {
    var allnodes = element.getElementsByTagName('*'),
        length = allnodes.length;
    var nodes = new Array(length);
    for (var k = 0; k < length; k++) {
        nodes[k] = allnodes[k];
    }
    for (var j = 0, l = nodes.length; j < l; j++) {
        var node = nodes[j];
        if (node.nodeType === 1) {
            if (node.dispose && typeof (node.dispose) === "function") {
                node.dispose();
            }
            else if (node.control && typeof (node.control.dispose) === "function") {
                node.control.dispose();
            }
            var behaviors = node._behaviors;
            if (behaviors) {
                behaviors = Array.apply(null, behaviors);
                for (var k = behaviors.length - 1; k >= 0; k--) {
                    behaviors[k].dispose();
                }
            }
        }
    }
}
  </script>
  <script type="text/javascript">
function viewerPropertyChanged(sender, e) {
    if (e.get_propertyName() == "isLoading") {
        if ($find("reportViewer").get_isLoading()) {
            // Do something when loading starts
        }
        else {
            // Do something when loading stops
        }
    }
};
var UPRO = {
   isDashboard: false,
   showHide: {params:{initial:false, current:false},splitter:{initial:false, current:false},toolbar:{initial:false, current:false}},
   zoomCell: null,
   scrollCell: null,
   wrapperCell: null,
   reportViewer: null,
   attachEvent: function(el, event, func){
      if (el.addEventListener) {
         el.addEventListener(event,func,false);
      } else if(el.attachEvent) {
         el.attachEvent('on'+event,func);
      } else {
         el['on'+event] = func;
      }
   },
   postMessage: function(command, extraArgs){
      if(!parent) return;
      var args = [command,window.name];
      if(extraArgs){
         for(var i=0; i<extraArgs.length; i++){
            args.push(extraArgs[i]);
         }
      }
      try{
         parent.postMessage(args.join("~"), "*");
      }catch(e){}
   },
   ReplaceNode: function(el){
      var elClone = el.cloneNode(true);
      el.parentNode.replaceChild(elClone, el);
      return elClone;
   },
   Debounce: function(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
   },
   FindInId: function(elementType, needle){
      var e = document.getElementsByTagName(elementType);
      for(var i=0; i<e.length; i++){
         if(e[i].id && e[i].id.indexOf(needle) > -1){
            return e[i];
         }
      }
      return null;
   },
   GetFirstElementOfType: function(e, type, maxDist, parent){
      if(maxDist != 0)maxDist=maxDist||1000;
      if(!e || !maxDist) return null;
      if(parent){
         if(e.parentNode){
            if(e.parentNode.tagName.toUpperCase() == type.toUpperCase()) return e.parentNode;
            var l = _this.GetFirstElementOfType(e.parentNode, type, --maxDist, true);
            if(l) return l;
         }
      }else{
         for(var i=0; i<e.children.length; i++){
            if(e.children[i].tagName.toUpperCase() == type.toUpperCase()) return e.children[i];
            var l = _this.GetFirstElementOfType(e.children[i], type, --maxDist);
            if(l) return l;
         }
      }
      return null;
   },
   FixLinkTargets: function(){
      var a = document.getElementsByTagName("a");
      for(var i=0; i<a.length; i++){
         if(a[i]){
            if(a[i].target && a[i].target == "_top")
               a[i].target = "_self";
         }
      }
   },
   PropertyChangeListener: function(sender, e){
      var name = e.get_propertyName(), isLoading = _this.reportViewer.get_isLoading();
      if (name == "isLoading") {
         if (isLoading) {
            // Do something when loading starts
         }
         else {
            _this.ShowHideControls(_this.showHide.toolbar.current);
            _this.Debounce(_this.FixLinkTargets, 100)();
         }
         return;
      }
      if(name == "zoomLevel")
         _this.FixZoom();
   },
   GetZoomNumber: function(num){
      if(num.indexOf("scale") > -1)
         num = num.replace(/[^\d\.]*/gi,"");
      var n = parseFloat(num);
      if(isNaN(n)) return false;
      if(num.indexOf("%") > -1){
         return n/100;
      }
      return n;
   },
   GetZoomCells: function(){
      _this.wrapperCell = _this.GetFirstElementOfType(_this.scrollCell, "div");
      var td = _this.GetFirstElementOfType(_this.wrapperCell, "td", 4);
      if(td){
         var table = _this.GetFirstElementOfType(td, "table", 3, true);
         if(table){
            _this.zoomCell = td.remove(true);
            table.remove();
            _this.wrapperCell.style.overflow = "hidden";
            _this.wrapperCell.appendChild(_this.zoomCell);
         }
         if(_this.zoomCell.childNodes.length > 1){
             var tb = document.createElement('table'); tb.setAttribute('border', '0');tb.setAttribute('cellpadding', '0');tb.setAttribute('cellspacing', '0');
             var tr = document.createElement('tr');
             var td = document.createElement('td');
             while(_this.zoomCell.childNodes.length > 0){
                 td.appendChild(_this.zoomCell.firstChild.remove(true));
             }
             tr.appendChild(td);
             tb.appendChild(tr);
             _this.zoomCell.appendChild(tb);
         }
      }
   },
   ApplyZoomStyles: function(zoom){
      _this.zoomCell.style.transform = "scale("+zoom+")";
      _this.zoomCell.style.zoom = "";
      _this.zoomCell.style.display = "block";
      _this.wrapperCell.style.removeProperty("width");
      _this.wrapperCell.style.removeProperty("height");
      var toolbar = document.getElementsByClassName("ToolBarBackground");
      var width = _this.GetFirstElementOfType(_this.zoomCell,"table",3).clientWidth*zoom, scrollWidth = _this.scrollCell.clientWidth;
      var height = _this.zoomCell.scrollHeight*zoom, scrollHeight = _this.wrapperCell.scrollHeight;
      console.log(width+" "+scrollWidth);
      _this.wrapperCell.style.width = (width <= scrollWidth ? scrollWidth : width) +"px";
      _this.wrapperCell.style.height = (height >= scrollHeight ? scrollHeight : height) + "px";  
   },
   FixZoom: function(){
     if(!_this.scrollCell) return;
     console.log("fixing zoom")
      var scroll = _this.scrollCell.scrollTop;
      _this.GetZoomCells();
      if(_this.zoomCell && _this.wrapperCell){
         _this.zoomCell.style.transformOrigin = "top left";
         var zoom = _this.GetZoomNumber(_this.zoomCell.style.zoom||_this.zoomCell.style.transform);
         var zoomLevel = _this.reportViewer.get_zoomLevel();
         if(zoomLevel == "PageWidth"){
            _this.ApplyZoomStyles(1);
            zoom = _this.scrollCell.clientWidth/_this.GetFirstElementOfType(_this.zoomCell,"table",3).clientWidth;
         }
         if(zoom){
            _this.ApplyZoomStyles(zoom);
            _this.scrollCell.scrollTop = scroll;
         }
         
      }
   },
   ShowHideControls: function(force){
      if(_this.reportViewer && _this.reportViewer.get_isLoading())
         return;
      var row, style = force===true ? "table-row": force===false ? "none" : "",params = document.getElementById("ParametersRowReportViewerControl"), toolbar = document.getElementsByClassName("ToolBarBackground"),splitter = document.getElementsByClassName("SplitterNormal")||document.getElementByClassName("SplitterHover");
      if(toolbar.length){
        var controls = toolbar[0].getElementsByClassName('ToolBarButtonsCell');
        if(controls.length){
            controls[0].style.width = "150%";
        }
      }
      var postMessage = style=="";
      if(_this.showHide.toolbar.initial && toolbar.length){
         row = _this.GetFirstElementOfType(toolbar[0], "tr", 2, true);
         if(row){
            if(style == ""){
               if(row.style.display == "none"){
                  style = "table-row";
               }else{
                  style = "none";
               }
            }
            row.style.display = style;
            _this.showHide.toolbar.current = (style != "none");
         }
      }
      if(_this.showHide.params.initial && params){
         if(style == ""){
            if(row.style.display == "none"){
               style = "table-row";
            }else{
               style = "none";
            }
         }
         params.style.display = style;
         _this.showHide.params.current = (style != "none");
      }
      if(_this.showHide.splitter.initial && splitter.length){
         row = _this.GetFirstElementOfType(splitter[0], "tr", 1, true);
         if(row){
            if(style == ""){
               if(row.style.display == "none"){
                  style = "table-row";
               }else{
                  style = "none";
               }
            }
            row.style.display = style;
           _this.showHide.splitter.current = (style != "none");
         }
      }
     if(postMessage) _this.postMessage("updateControls", [(style == "none")]);
     var evt = document.createEvent('UIEvents');
     evt.initUIEvent('resize', true, false, window, 0);
     window.dispatchEvent(evt);
   },
   InitDashboardReports: function(){
      _this.isDashboard = true;
      //window.getComputedStyle = null;
      HTMLElement.prototype.remove = function() { var p = this.parentNode; p.removeChild(this); return this; }
      var row,params = document.getElementById("ParametersRowReportViewerControl"), toolbar = document.getElementsByClassName("ToolBarBackground"),splitter = document.getElementsByClassName("SplitterNormal")||document.getElementByClassName("SplitterHover");
      if(toolbar.length){
         row = _this.GetFirstElementOfType(toolbar[0], "tr", 2, true);
         if(row && row.style.display != "none"){
            _this.showHide.toolbar.initial = _this.showHide.toolbar.current = true;
         }
      }
      if(params && params.style.display != "none"){
         _this.showHide.params.initial = _this.showHide.params.current = true;
      }
      if(splitter.length){
         row = _this.GetFirstElementOfType(splitter[0], "tr", 1, true);
         if(row && row.style.display != "none"){
            _this.showHide.splitter.initial = _this.showHide.splitter.current = true;
         }
      }
      try {
         Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {
            document.getElementById("ReportViewerControl_fixedTable").style.tableLayout = 'auto';
            var div = _this.FindInId("div", "VisibleReportContentReportViewerControl");
            if(div){
                  var top = 0, row = _this.GetFirstElementOfType(div, "tr", 3);
                  if(row){
                     top = row.previousSibling.getBoundingClientRect().bottom;
                  }
                  _this.scrollCell = div;
                  div.parentNode.style.width = '';
                  div.parentNode.style.height = '';
                  div.parentNode.style.left = '0px';
                  div.parentNode.style.top = '0px';
                  div.parentNode.style.right = '0px';
                  div.parentNode.style.bottom = '0px';
                  div.parentNode.style.position = 'absolute';
                  div.parentNode.style.overflow = 'auto';
                  var parent = div.parentNode.parentNode;
                  var container = document.createElement("div");
                  container.style.position = "relative";
                  container.style.height = "100%";
                  container.appendChild(div.parentNode.remove());
                  parent.appendChild(container);
            }
         });
      } catch(e) {
        console.log(e);
      }
      try {
         Sys.Application.add_load(function () {
            _this.reportViewer = $find("ReportViewerControl");
            _this.attachEvent(window, "resize", 
               _this.Debounce(function() {
                  _this.FixZoom();
               }, 250), true);
            _this.reportViewer.add_propertyChanged(_this.PropertyChangeListener);
         });
      } catch(e){}
   },
   CheckForDashboard: function(){
      if(parent){
         _this = this;
         _this.attachEvent(window, "message", function (event){
             if(typeof event.data != 'string') return;
            var origin = event.origin || event.originalEvent.origin, data = event.data.split("~");
            switch(data[0]){
               case "iAmUPRO":
                  _this.InitDashboardReports();
                  if(data[1])
                     _this.ShowHideControls(data[1]!=="true");
               break;
               case "showControls":
                  _this.ShowHideControls();
               break;
            }
         }, false);
         _this.postMessage("areYouUPRO");
      }
   }
}
UPRO.CheckForDashboard();
</script>
 </body>
</html>
